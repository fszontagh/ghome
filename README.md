## INSTALL:

#### INSTALL/UPDATE NODE:
```bash
curl -sL https://deb.nodesource.com/setup_6.x -o nodesource_setup.sh
chmod 777 nodesource_setup.sh
./nodesource_setup.sh
apt-get install nodejs
```

#### INSTALL PHP5.6 fcgi
```bash
add-apt-repository ppa:ondrej/php
apt-get update
apt-get install php5.6-sqlite3 php5.6-cgi php5.6-mbstring php5.6-odbc
```

#### INSTALL FILES:
##### - download files:
```bash
cd /opt
git clone https://fszontagh@bitbucket.org/fszontagh/ghome.git
```

##### - install node modules:
```bash
./install_node_deps.sh
(This script will install the node packages with npm)
```

##### - change the settings:
* If you use the default paths (/opt/ghome), then *__no need modifications__*
* Otherwise, you need to edit the following files:
 -web/config.php
 -start.js (on line 22.)


##### - configure mqtt:
* Edit the mqtt.js, change the "mqtt://<hostname>" to your mqtt server
* Open the web/data/database.sqlite with a sqlite3 editor (http://sqlitebrowser.org/), and change the values in the settings table
 
 
#### To start the web interface, run the following command:
```bash
nodejs start.js web start
```
 
#### To start the mqtt client, run the following command: 
```bash
nodejs start.js mqtt start
```
 
When everything is working, you can access the web interface on localhost port 8881 (http://hostname:8881)

