var express = require('express');
var php = require('express-php');
var app = express();

app.use(php.cgi('./web','/usr/bin/php-cgi -q'));
app.use(express.static('./web'));

app.listen(8881);
