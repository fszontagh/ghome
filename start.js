#!/usr/bin/nodejs


var argv = process.argv;

if (!argv[2] || !argv[3]) {
    var err_str = "Usage: " + argv[1] + " param cmd\n";
    err_str += "Params:\n";
    err_str += "\tweb - control the webserver\n";
    err_str += "\tmqtt - control the mqtt listener server\n";
    err_str += "Commands: \n";
    err_str += "\tstart stop status\n";
    console.error(err_str);
    process.exit();
}

var server = argv[2];
var cmd = argv[3];


//change working directory
process.chdir('/opt/ghome');
//change working directory


var start_process = function (srv) {
    var fs = require('fs');
    var spawn = require('child_process').spawn;
    var start = spawn('node', [srv + '.js']);
    console.info(srv + " stared, pid: " + start.pid);
    fs.writeFileSync(srv + ".pid", start.pid);
}


var stop_process = function (srv) {
    var fs = require('fs');
    var pid = fs.readFileSync(srv + ".pid");

    const fkill = require('fkill');
    fkill(pid).then(() => {
        console.log('process stoped');
    });
}

var status_process = function (srv) {
    var fs = require('fs');
    var path = srv + ".pid";
    if (fs.existsSync(path) === false) {
        console.info("pid file not found: " + path);
        return false;
    }
    var pid = fs.readFileSync(path);
    if (require('is-running')(pid) === false) {
        console.info("process not running, deleting pid file...");
        fs.unlinkSync(path);
        return false;
    }
    console.info(srv + " pid: " + pid);
    return true;

}


if (server === 'web' || server === 'mqtt') {
    var status = status_process(server);
    if (cmd === 'start') {
        if (status === false) {
            start_process(server);
            status_process(server);
        }
    }
    if (cmd === 'stop') {
        if (status === true) {
            stop_process(server)
            status_process(server);
        }
    }

    //if (cmd === "status") {
    //    status_process(server);
    // }
}

process.exit();
/*
 
 var spawn = require('child_process').spawn;
 var test2 = spawn('node', ['server.js']);
 
 
 
 fs.readFileSync("");
 console.log(test2.pid);
 
 fs.writeFile("server.pid", test2.pid, function (err) {
 if (err) {
 return console.log(err);
 }
 
 console.log("Server started");
 }); 
 */
