<?php

$config = include_once 'config.php';
require_once "libs/form2.class.php";
require_once 'libs/template.class.php';
require_once 'libs/navigation.class.php';
require_once './libs/menu.class.php';
require_once './libs/devicehistory.class.php';
require_once 'libs/graph.class.php';


$tpl = new template("template/default/");
$menu = new menu();



$nav = new navigation();

$menu->addMenu("Dashboard", $nav->generateUrl("home"), $nav->isActive("home"), 1);
$menu->addMenu("Devices", $nav->generateUrl("config"), $nav->isActive("config"), 1);

if (file_exists("pages/mqtt.php")) {
    $menu->addMenu("MQTT", $nav->generateUrl("mqtt"), $nav->isActive("mqtt"), 1);
}
if (file_exists("pages/motioneye.php")) {
    $menu->addMenu("ME", $nav->generateUrl("motioneye"), $nav->isActive("motioneye"), 1);
}
if (file_exists("pages/camera.php")) {
    $menu->addMenu("Camera", $nav->generateUrl("camera"), $nav->isActive("camera"), 1);
}

    $menu->addMenu("Automation", $nav->generateUrl("events"), $nav->isActive("events"), 2);


$db = new PDO('sqlite:' . $config["database"]);
$db->exec('PRAGMA journal_mode = wal;');
//$db->exec("CREATE TABLE users (Id INTEGER PRIMARY KEY, username TEXT, name TEXT, Age INTEGER, password TEXT)");  
//$db->exec("insert into users (username,name,age,password)values('admin','administrator','0','admin')");
$data = array();
if (isset($_GET['p']) AND ! empty($_GET['p'])) {
    $page = htmlspecialchars($_GET['p']);
    $p = "pages/" . $page . ".php";
    if (file_exists($p)) {
        $data = include_once $p;
    } else {
        $data = include_once 'pages/404.php';
    }
} else {
    
}
//header
$tpl->add("url", $nav->cur);

$menulist = $menu->listMenu();
//echo "<pre>".print_r($menulist,true)."</pre>";

$tpl->add("menu", $menulist);



$tpl->add($data);
$tpl->show("header.html");
$tpl->add($data);

if (isset($data['tpl'])) {
    $tpl->show($data['tpl']);
}
$tpl->show("footer.html");