<?php

require("../libs/mqtt.class.php");
require("../libs/devicehistory.class.php");
$config = include_once '../config.php';
$db = new PDO('sqlite:' . $config["database"]);

$dh = new devicehistory($config);

$mqtt_settings = $db->query("SELECT * from settings where name LIKE 'mqtt_%'");

while ($row = $mqtt_settings->fetch(PDO::FETCH_ASSOC)) {
    $mqtt_broker[$row["name"]] = $row["data"];
}
if ($config["debug"] === true) {
    print_r($mqtt_broker);
}

$server = $mqtt_broker["mqtt_broker_ip"];     // change if necessary
$port = $mqtt_broker["mqtt_broker_port"];                     // change if necessary
$username = $mqtt_broker["mqtt_broker_username"];                   // set your username
$password = $mqtt_broker["mqtt_broker_password"];                   // set your password
$client_id = $config['mqtt_client_id']; // make sure this is unique for connecting to sever - you could use uniqid()

$mqtt = new phpMQTT($server, $port, $client_id);

if (!$mqtt->connect(true, NULL, $username, $password)) {
    exit(1);
}
//$topics = array();
$query = $db->query("SELECT topic,device_id FROM mqtt_devices WHERE enabled = 1 ORDER BY device_id DESC");
if ($config["debug"] === true) {
    echo "Found topics: \n";
}
$topicdevid = array();
while (($row = $query->fetch(PDO::FETCH_ASSOC)) !== false) {
    if (!isset($row["topic"])) {
        continue;
    }
    if ($config["debug"] === true) {
        echo $row['topic'] . "\n";
    }
    $topicdevid[$row['topic']] = $row["device_id"];
    $topics[$row["topic"]] = array("qos" => 0, "function" => "procmsg");
}


if (count($topics) < 1) {
    echo "\nNo topics, i can't run...\n";
    exit(1);
}
$mqtt->subscribe($topics, 0);

while ($mqtt->proc()) {
    if (file_exists($config["mqtt_command_file"])) {
        $cmd = file_get_contents($config["mqtt_command_file"]);
        if ($cmd == "stop") {
            unlink($config["mqtt_command_file"]);
            exit;
        }
    }
    usleep(1);
}


$mqtt->close();

function procmsg($topic, $msg) {
    GLOBAL $db, $config,$dh,$topicdevid;
    $sql = "update mqtt_devices SET last_update = " . time() . ", response = '" . trim($msg) . "' WHERE topic = '" . trim($topic) . "'";
    $dh->addHistoryItem($topicdevid[$topic], $msg);
    if ($config["debug"] === true) {
        echo "Msg Recieved: " . date("r") . "\n";
        echo "Topic: {$topic}\n\n";
        echo "\t$msg\n\n";
        echo "SQL: \n";
        echo $sql;
        echo "\n";
    }

    $db->exec($sql);
    $err = $db->errorInfo();
    if (isset($err[2]) AND ! empty($err[2])) {
        echo "[SQL ERROR]: " . $err[2] . "\n";
    }
}
