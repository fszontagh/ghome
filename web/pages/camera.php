<?php

if (isset($_GET['device']) AND is_numeric($_GET['device'])) {


    $devid = intval($_GET['device']);
    if ($devid > 0) {
        $devq = $db->query("SELECT video_devices.*, devices.name FROM video_devices, devices WHERE video_devices.device_id = devices.id AND video_devices.id = " . $devid . " ORDER BY id DESC");
        $dev = $devq->fetch(PDO::FETCH_ASSOC);
    }

    $f = new form2("video_device");
    $type = $f->addInput("select", "type", "Type:", array("html5" => "HTML5", "mjpeg" => "MJPEG"));

    if (isset($dev)) {
        $type->addAttr("selected", $dev['type']);
    }

    $source = $f->addInput("text", "source", "Source url: ")->addAttr("required", "required");
    if (isset($dev)) {
        $source->addAttr("value", $dev['source']);
    }


    $me_devicesq = $db->query("SELECT name,id from devices WHERE type = 'video'");
    $me_devices = $me_devicesq->fetchAll(PDO::FETCH_ASSOC);
    $mdevices[""] = "Please select one";
    foreach ($me_devices as $idx => $device) {
        $mdevices[$device["id"]] = $device['name'];
    }
    $device_id = $f->addInput("select", "device_id", "Device: ", $mdevices);

    if (isset($dev)) {
        $device_id->addAttr("selected", $dev['device_id']);
    }

    $f->addButton("save", "save");
    $f->addResetButton("cancel", "Cancel")->addAttr("onclick", "window.location='" . $nav->generateUrl("camera") . "';");
    $data['tpl'] = "form.html";
    $data['title'] = "Video device";


    if ($f->validate() === true) {
        if (isset($dev)) {
            $db->exec("update video_devices set type='" . $type->value() . "', source='" . $source->value() . "', device_id = " . $device_id->value());
        } else {
            $db->exec("insert into video_devices (type,source,device_id)values('" . $type->value() . "', '" . $source->value() . "'," . $device_id->value() . ")");
        }
        $err = $db->errorInfo();
        if (!empty($err[2])) {
            echo $err[2];
        } else {
            $nav->redirect("camera");
        }
    }
    $data["form"] = $f->show();
    return $data;
}


$devicesq = $db->query("SELECT video_devices.*, devices.name FROM video_devices, devices WHERE video_devices.device_id = devices.id ORDER BY id DESC");
$devices = $devicesq->fetchAll(PDO::FETCH_ASSOC);

$data['title'] = "Video devices";
$data['tpl'] = "camera.html";
$data["devices"] = $devices;

return $data;
