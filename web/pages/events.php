<?php

//add new trigger
if (isset($_GET['trigger']) AND is_numeric($_GET['trigger'])) {

    $f = new form2("addTrigger");

    /* get the list of the available devices */
    $me_devicesq = $db->query("SELECT "
            . "devices.name, "
            . "devices.id, "
            . "devices.type, "
            . "devices_group.name as gname, "
            . "devices_group.id as gid "
            . "FROM devices, devices_group "
            . "WHERE devices.`group` = devices_group.id "
            . "ORDER BY devices.name DESC");


    $errinfo = $db->errorInfo();
    if (isset($errinfo[2]) AND ! empty($errinfo[2])) {
        echo $errinfo[2];
    }

    $me_devices = $me_devicesq->fetchAll(PDO::FETCH_ASSOC);
    $mdevices[""] = "Please select one";
    foreach ($me_devices as $idx => $device) {
        $mdevices[$device["id"]] = $device['name'] . " (" . $device['type'] . ") - " . $device['gname'];
    }

    $name = $f->addInput("text", "name", "Trigger name:")->addAttr("required");
    $name->addAttr("placeholder", "Somebody activated the motion detection...");

    $dev_id = $f->addInput("select", "device_id", "Device:", $mdevices)->addAttr("required", "true");


    $dev_val = $f->addInput("text", "device_value", "Device value:")->addAttr("required", "true");


    $sleep = $f->addInput("number", "delay", "Delay <em>(in seconds)</em>:", 0);
    $sleep->addAttr(array(
        "min" => 0,
        "max" => 600,
        "step" => 1,
        "required"
    ));

    $f->addButton("save", "Save");
    $f->addResetButton("cancel", "Cancel")->addAttr("onclick", "window.location='" . $nav->generateUrl("events") . "';");

    $data['title'] = 'Add new event - Trigger';
    $data['tpl'] = "form.html";
    $data['form'] = $f->show();

    //save first step
    if ($f->validate() === true) {
        //$db->exec("INSERT INTO automation_trigger (device_id,name,device_value,delay) values ('".$dev_id->value()."', '".$name->value()."' )";
        $db->exec("INSERT INTO automation_trigger (device_id,name,device_value, delay) values (" . $dev_id->value() . ", '" . $name->value() . "','" . $dev_val->value() . "', " . $sleep->value() . ")");
        $nav->redirect("events&action=0");
    }

    return $data;
}
//add new action
if (isset($_GET['action']) AND is_numeric($_GET['action'])) {
    $f = new form2("action");
    $name = $f->addInput("text", "action_name", "Action name:");
    //trigger list
    $triggerq = $db->query("SELECT automation_trigger.name,automation_trigger.id, devices.name as device_name FROM automation_trigger, devices WHERE automation_trigger.device_id = devices.id ORDER BY automation_trigger.name DESC");

    $errinfo = $db->errorInfo();
    if (isset($errinfo[2]) AND ! empty($errinfo[2])) {
        echo $errinfo[2];
    }

    $tr = $triggerq->fetchAll(PDO::FETCH_ASSOC);
    $triggers = array();
    $triggers[""] = "Please select one";
    foreach ($tr as $idx => $trig) {
        $triggers[$trig['id']] = $trig['name'] . " (Device: " . $trig['device_name'] . ") ";
    }
    $trigger = $f->addInput("select", "trigger", "Trigger", $triggers);

    $trigger->addAttr('required');

    $object = $f->addInput("select", "object", "Object to control:", array(
        "" => "Please select one",
        "group" => "Group",
        "device" => "Device"
    ));


    $gettypesq = $db->query("SELECT type FROM devices WHERE dtype = 'switch' GROUP BY dtype");
    $types = array();

    foreach ($gettypesq->fetchAll(PDO::FETCH_ASSOC) as $idx => $type) {
        $types[$type['type']] = $type['type'];
    }
    $dtype = $f->addInput("select", "connection_type", "Connection type: ", $types);

    $object->addAttr('required');

    $sleep = $f->addInput("number", "delay", "Delay <em>(in seconds)</em>:", 0);
    $sleep->addAttr(array(
        "min" => 0,
        "max" => 600,
        "step" => 1,
        "required"
    ));


    $f->addButton("save", "Save");
    $f->addResetButton("cancel", "Cancel")->addAttr("onclick", "window.location='" . $nav->generateUrl("events") . "';");

    //save
    if ($f->validate() === true) {
        $db->exec("insert into automation_action (object,trigger_id,delay,connection_type,name) values ('" . $object->value() . "'," . $trigger->value() . "," . $sleep->value() . ", '" . $dtype->value() . "', '" . $name->value() . "')");
        $id = $db->lastInsertId();
        $nav->redirect("events&select_device=" . $id);
    }


    $data['title'] = "Add action";
    $data['tpl'] = "form.html";
    $data['form'] = $f->show();
    return $data;
}


//select device to action
if (isset($_GET['select_device']) AND is_numeric($_GET['select_device']) AND $_GET['select_device'] > 0) {
    $action_id = intval($_GET['select_device']);
    $actionq = $db->query("SELECT object, trigger_id FROM automation_action WHERE id = " . $action_id);
    $action = $actionq->fetch(PDO::FETCH_ASSOC);


    $triggerq = $db->query("SELECT name, id, device_id FROM automation_trigger WHERE id = " . $action['trigger_id']);
    $trigger = $triggerq->fetch(PDO::FETCH_ASSOC);
    //if the selected object is switch
    if ($action['object'] == 'device') {
        $f = new form2("select_object");
        $connections = array();
        $connections[""] = "Please select one";
        //$devicesq = $db->query("SELECT devices.id,devices.name, devices_group.name as gname, devices.type FROM devices, devices_group WHERE devices.`group` = devices_group.id AND devices.dtype = 'switch' ORDER BY devices.name DESC ");
        $connectionsq = $db->query("SELECT devices.name, mqtt_devices.id as mid ,mqtt_devices.* FROM mqtt_devices, devices WHERE mqtt_devices.device_id = devices.id");


        $connectionsr = $connectionsq->fetchAll(PDO::FETCH_ASSOC);
        foreach ($connectionsr as $idx => $connection) {
            $connections[$connection['id']] = $connection["name"] . ": " . $connection['topic'] . " (on_state_data: " . $connection['on_state_value'] . " off_state_data: " . $connection['off_state_value'] . ")";
        }

        $conn_id = $f->addInput("select", "device_id", "Device: ", $connections)->addAttr("required", "true");

        $dev_data = $f->addInput("select", "device_data", "Data to send to the device: ", array(
            "" => "Please select one",
            "on_state_value" => "On state value",
            "off_state_value" => "Off state value"
        ));
        $dev_data->addAttr("required", "true");
        //$dev_data->addAttr("placeholder","On mqtt: OFF - to turn off the device");
        $f->addButton("save", "Save");

        if ($f->validate() === true) {
            $sql = "UPDATE automation_action SET device_data = '" . $dev_data->value() . "', connection_id = " . $conn_id->value() . " WHERE id = " . $action_id;
            $db->exec($sql);

            $errinfo = $db->errorInfo();
            if (isset($errinfo[2]) AND ! empty($errinfo[2])) {
                echo $sql . ": " . $errinfo[2];
            } else {
                $nav->redirect("events");
            }
        } else {
            $data['form'] = $f->show();
        }
    } else {
        $data['form'] = "<p>Sorry, the 'group action' not implemented yet :/</p>";
    }

    $data['title'] = "Select device to action";
    $data["tpl"] = "form.html";

    return $data;
}


//list triggers
$triggerq = $db->query('SELECT automation_trigger.*, devices.name as device_name FROM automation_trigger, devices WHERE automation_trigger.device_id = devices.id ORDER BY automation_trigger.id ASC');
$errinfo = $db->errorInfo();
if (isset($errinfo[2]) AND ! empty($errinfo[2])) {
    echo $errinfo[2];
}
$trig = array();
foreach ($triggerq->fetchAll(PDO::FETCH_ASSOC) as $idx => $trigger) {

    $actionsq = $db->query("SELECT automation_action.*, mqtt_devices.device_id,mqtt_devices.topic FROM automation_action, mqtt_devices WHERE automation_action.connection_id = mqtt_devices.id AND automation_action.trigger_id = " . $trigger['id']." ORDER BY priority ASC");

    $errinfo = $db->errorInfo();
    if (isset($errinfo[2]) AND ! empty($errinfo[2])) {
        echo $errinfo[2];
    }

    $trigger["actions"] = $actionsq->fetchAll(PDO::FETCH_ASSOC);

    $trig[] = $trigger;
}

$data['triggers'] = $trig;
//$data['triggers'] = $triggerq->fetchAll(PDO::FETCH_ASSOC);


$data["title"] = "Automation settings";
$data['tpl'] = "events.html";
return $data;
