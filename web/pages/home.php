<?php

$data = array();
$data["title"] = "Home";
$q = $db->query("SELECT "
        . "devices.type, "
        . "devices.dtype, "
        . "devices.`group`, "
        . "devices.name, "
        . "devices.id, "
        . "devices_group.id as did, "
        . "devices_group.name as groupname "
        . "FROM devices, devices_group "
        . "WHERE "
        . "devices.`group` = devices_group.id "
        . "ORDER BY devices.name DESC");
$err = $db->errorInfo();
if (isset($err[2]) AND ! empty($err[2])) {
    echo "[SQL ERROR]: " . $err[2] . "\n";
}
$devices = array();



while ($r = $q->fetch(PDO::FETCH_ASSOC)) {
    $r["status"] = "";
    $r["last_update_f"] = "";
    $r["last_update"] = time();
    if ($r["type"] == "mqtt") {
        $mqtt_q = $db->query("SELECT `last_update`,`response` from mqtt_devices where device_id = " . $r["id"] . " ");
        $status = $mqtt_q->fetch(PDO::FETCH_ASSOC);
        $r["last_update_f"] = date("Y-m-d H:i:s", $status['last_update']);
        $r["status"] = $status["response"];
        $r['last_update'] = $status['last_update'];
        unset($status);
    }
    if ($r['type'] == "motioneye") {
        $meq = $db->query("SELECT `time`, `status` from motioneye_devices WHERE device_id = " . $r['id']);
        $status = $meq->fetch(PDO::FETCH_ASSOC);
        $r['last_update_f'] = date("Y-m-d H:i:s", $status['time']);
        $r['last_update'] = $status['time'];
        $r['status'] = $status['status'];
        unset($status);
    }
    $devices[] = $r;
}


$data["tpl"] = "home.html";
$data["devices"] = $devices;
return $data;
