<?php

$data = array();

$data['title'] = "Config";
$data["tpl"] = "config.html";

$devices = $db->query("select * from devices order by name desc")->fetchAll();
$groups = $db->query("SELECT * FROM devices_group ORDER BY name DESC")->fetchAll();

foreach ($devices as $did => $device) {
   if ($device["group"]==0) {
       $gname = "No Group";
   }else{
       $gname = $db->query("select name from devices_group where id = ".$device["group"])->fetchAll()[0]["name"];
   }
    
   // $gname = $groups[$device["group"]]['name'];
    $devices[$did]['group_name'] = $gname;
}

$data["devices"] = $devices;

if (isset($_GET['device']) AND ( $_GET['device'] == 0 OR $_GET["device"] > 0)) {
    $device_data = array("name" => "", "type" => "", "id" => 0, "group" => 0,"dtype"=>"none");
    if ($_GET["device"] > 0) {
        $device_data = $db->query("select * from devices where id = " . intval($_GET['device']))->fetchAll()[0];
    }
    $f = new form2("addDevice");
    $name = $f->addInput("text", "device_name", "Name: ", $device_data['name']);
    $name->addAttr("required", "true");
//todo: list it from the database
    
    $device_type_data = array(
        "" => "Please select one",
        "mqtt" => "MQTT",
        "webapi" => "Web API",
        "video"=>"Video",
        "mobile_phone_tracking"=>"Mobile Phone tracking",
        "pc"=>"PC",
        "motioneye"=>"MotionEye trigger"
    );

    
    $device_dtype_data = array(
        "none"=>"Empty device",
        "switch"=>"Power switch",
        "sensor"=>"Sensor"
    );
    
    $type = $f->addInput("select", "device_source_type", "Data Source Type:", $device_type_data);
    $dtype = $f->addInput("select", "ddevice_type", "Type:", $device_dtype_data);


    $gd = array();
    $gd[0] = "No group";
    foreach ($groups as $gid => $group) {
        $gd[$group["id"]] = $group["name"];
    }
    //echo "<pre>" . print_r($groups, true) . "</pre>";

    $group = $f->addInput("select", "category", "Category:", $gd);
    $group->addAttr("selected", $device_data['group']);
    $type->addAttr("selected", $device_data['type']);
    $dtype->addAttr("selected", $device_data['dtype']);
    $type->addAttr("required", "true");
    $f->addButton("add_device", "Save");
    $f->addResetButton("reset", "Back")->addAttr("onclick='window.location=\"?p=config\"'");

    if ($f->validate() === true) {
        if ($device_data["id"] == 0) {
            $db->exec("insert into devices (name,type,`group`,dtype)values('" . $name->value() . "','" . $type->value() . "'," . $group->value() . ",'".$dtype->value()."')");
        } else {
            $db->exec("update devices set `group` = " . $group->value() . ", name = '" . $name->value() . "', type = '" . $type->value() . "', dtype = '".$dtype->value()."' where id = " . $device_data['id'] . " ");
        }
        
        $err = $db->errorInfo();
        if (isset($err[2]) AND strlen($err[2])>1) {
            $data["error"] = $err[2];
        }else{
            $nav->redirect("config");
        }
    }

    $data["form"] = $f->show();
    $data["tpl"] = "form.html";
    $data['title'] = "Config - add new device";
}




return $data;
