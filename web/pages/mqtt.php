<?php

if (isset($_GET['mqtt_device_del']) AND is_numeric($_GET['mqtt_device_del'])) {
    $db->exec("DELETE from mqtt_devices WHERE id = " . intval($_GET['mqtt_device_del']));
    $nav->redirect("mqtt");
}
if (isset($_GET['mqtt_device']) AND is_numeric($_GET['mqtt_device'])) {
    $id = intval($_GET['mqtt_device']);
    $f = new form2("mqtt_device");


    if ($id > 0) {
        $connq = $db->query("SELECT * FROM mqtt_devices WHERE id = " . $id);
        $connection = $connq->fetch(PDO::FETCH_ASSOC);
    }
    $topic = $f->addInput("text", "topic", "Topic:", isset($connection['topic']) ? $connection['topic'] : null);
    $topic->addAttr(array(
        "placeholder" => "stat/topic/POWER",
        "required" => "true"
    ));

    $devices_q = $db->query("select id, name from devices where type='mqtt'");
    $devices[""] = "Please select one device";
    foreach ($devices_q as $idx => $device) {
        $devices[$device["id"]] = $device['name'];
    }
    $dev = $f->addInput("select", "device", "Device: ", $devices);
    if ($id > 0) {
        $dev->addAttr("selected", $connection['device_id']);
    }


    //onstate value
    $onstate = $f->addInput("select", "on_state_value", "On state value:", array("ON" => "ON", "TRUE" => "TRUE","number"=>"Number > 0"));
    if ($id > 0) {
        $onstate->addAttr("selected", $connection['on_state_value']);
    }
    //offstate value
    $offstate = $f->addInput("select", "off_state_value", "Off state value:", array("OFF" => "OFF", "FALSE" => "FALSE","numner"=>"Number = 0"));
    if ($id > 0) {
        $offstate->addAttr("selected", $connection['off_state_value']);
    }

    $controller = $f->addInput("checkbox", "controller", "This topic can controll the devie?", 1);
    if ($id>0) {
        if ($connection["controller"]=='1') {
            $controller->addAttr("checked","checked");
        }
    }


    $enabled = $f->addInput("checkbox", "enabled", "Enabled", 1);
    if ($id > 0) {
        if ($connection["enabled"] == 1) {
            $enabled->addAttr("checked", "checked");
        }
    }

    $f->addButton("save", "Save");
    $f->addResetButton("cancel", "Cancel")->addAttr("onclick", "window.location='" . $nav->generateUrl("mqtt") . "';");

    $data["tpl"] = "form.html";
    if ($id > 0) {
        $data["title"] = "Edit connection";
    } else {
        $data["title"] = "Add connection";
    }

    //save data
    if ($f->validate() === true) {
        if ($id > 0) {
            $db->exec("update mqtt_devices set topic = '" . $topic->value() . "',enabled = " . $enabled->value() . ", device_id =  " . $dev->value() . ", on_state_value = '".$onstate->value()."', off_state_value = '".$offstate->value()."', controller = '".$controller->value()."' where id = " . $id);
        } else {
            $db->exec("insert into mqtt_devices (topic,enabled,device_id) values ('" . $topic->value() . "'," . $enabled->value() . "," . $dev->value() . ")");
        }
        $nav->redirect('mqtt');
    }
    //save data

    $data["form"] = $f->show();
    return $data;
}


$q = $db->query("SELECT "
        . "mqtt_devices.device_id,"
        . "mqtt_devices.id, "
        . "mqtt_devices.topic,"
        . "mqtt_devices.response, "
        . "mqtt_devices.last_update, "
        . "mqtt_devices.enabled, "
        . "devices.name "
        . "FROM mqtt_devices, devices WHERE mqtt_devices.device_id = devices.id");

if ($q === false) {
    echo $db->errorInfo()[2];
}
$mqtt = $q->fetchAll(PDO::FETCH_ASSOC);

foreach ($mqtt as $idx => $m) {
    $mqtt[$idx]["last_update_m"] = date("Y-m-d H:i:s", $m["last_update"]);
}

$data["mqtt"] = $mqtt;
$data["title"] = "MQTT connections";
$data["tpl"] = "mqtt.html";
return $data;
