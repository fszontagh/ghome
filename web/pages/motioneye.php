<?php

$sql = "SELECT "
        . "motioneye_devices.time, "
        . "motioneye_devices.id, "
        . "motioneye_devices.device_id, "
        . "motioneye_devices.status, "
        . "motioneye_devices.hash, "
        . "devices.name "
        . "FROM motioneye_devices, devices "
        . "WHERE "
        . "devices.id = motioneye_devices.device_id ";
//get devices
$devicesq = $db->query($sql);

$err = $db->errorInfo();
if (isset($err[2]) AND ! empty($err[2])) {
    echo "[SQL ERROR]: " . $err[2] . "\n";
}
$devices = $devicesq->fetchAll(PDO::FETCH_ASSOC);
if (isset($_GET['url']) AND is_numeric($_GET['url'])) {
    $did = intval($_GET['url']);
    $data["title"] = "URL to the motioneye device";
    $data["tpl"] = "form.html";
    $f = new form2("motioneyeurl");
    $hook = $f->addInput("text","web_hook_url","Web hook url:",$nav->getBase("jsapi.php?me=1&hash=".md5($did)));
    $hook->addAttr("disabled","disabled");
    $data['form'] = $f->show();
    return $data;
}
if (isset($_GET['device']) AND is_numeric($_GET["device"])) {
    $f = new form2("motioneyedevice");

    $me_devicesq = $db->query("SELECT name,id from devices WHERE type = 'motioneye'");
    $me_devices = $me_devicesq->fetchAll(PDO::FETCH_ASSOC);
    $mdevices[""]="Please select one";
    foreach ($me_devices as $idx => $device) {
        $mdevices[$device["id"]] = $device['name'];
    }
    $did = $f->addInput("select", "device_id", "Device: ", $mdevices);
    $did->addAttr("required","required");
    $f->addButton("save", "save");
    $f->addResetButton("cancel", "Cancel")->addAttr("onclick", "window.location='" . $nav->generateUrl("motioneye") . "';");
    $data["title"] = "Add device";
    $data['tpl'] = "form.html";

    if ($f->validate() === true) {
        $db->exec("insert into motioneye_devices (device_id) values (" . $did->value() . ") ");
        $err = $db->errorInfo();
        if (isset($err[2]) AND ! empty($err[2])) {
            echo "[SQL ERROR]: " . $err[2] . "\n";
        }

        $id = $db->lastInsertId();
        $hash = md5($id);
        $db->exec("update motioneye_devices set hash = '" . $hash . "' where id = " . $id);
        $err = $db->errorInfo();
        if (isset($err[2]) AND ! empty($err[2])) {
            echo "[SQL ERROR]: " . $err[2] . "\n";
        }
        $nav->redirect("motioneye");
    }


    $data['form'] = $f->show();
    return $data;
}




$data['title'] = "MotionEye devices";
$data['devices'] = $devices;

$data['tpl'] = "motioneye.html";

return $data;
