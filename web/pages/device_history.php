<?php

if (isset($_GET['device_id']) AND is_numeric($_GET['device_id'])) {
    $dh = new devicehistory($config);
    $device_id = intval($_GET['device_id']);
    
    $q = $dh->listHistory($device_id, " order by time desc ", "");

    if (!is_string($q)) {
        $info = $q->fetchAll(PDO::FETCH_ASSOC);
      
        foreach ($info as $idx => $data) {
            $info[$idx]["time_f"] = date("Y-m-d H:i:s",$info[$idx]['time']);
        }
        $data["history"] = $info;
    } else {
        $data["history"] = $q;
    }
   


    $q = $db->query("SELECT name FROM devices WHERE id = " . $device_id);
    $device = $q->fetch(PDO::FETCH_ASSOC);

    $data["title"] = "Device history: " . $device['name'];
    $data['tpl'] = "device_history.html";
    return $data;
}