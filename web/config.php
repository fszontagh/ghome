<?php

$cfg["root"] = "/opt/ghome/web";
$cfg["data"] = $cfg["root"] . DIRECTORY_SEPARATOR . "data";
$cfg = array(
    "database" => $cfg["data"] . DIRECTORY_SEPARATOR . "/database.sqlite", //sql database file
    "debug" => false, //turn on debug outputs?
    "mqtt_command_file" => $cfg["data"] . DIRECTORY_SEPARATOR . "mqtt_command", //command file for the mqtt listener
    "mqtt_client_id" => "gH0StHome",
    "history_database" => $cfg["data"] . DIRECTORY_SEPARATOR . "/history.sqlite"
);



return $cfg;
