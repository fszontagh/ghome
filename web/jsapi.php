<?php

header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
//header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

$config = include_once 'config.php';
if (isset($_GET["device_status"]) AND ! empty($_GET["device_status"]) AND is_numeric($_GET['device_status'])) {

    $db = new PDO('sqlite:' . $config["database"]);
    $device_id = intval($_GET["device_status"]);
    $qd = $db->query("SELECT type,name,id FROM devices WHERE id = " . $device_id);
    $device = $qd->fetch(PDO::FETCH_ASSOC);
    //mqtt devices
    if ($device["type"] == "mqtt") {
	$sql_cmd = "SELECT response as device_status,last_update,device_id FROM mqtt_devices WHERE device_id = " . $device_id;
        $sq = $db->query($sql_cmd);
        $status = $sq->fetch(PDO::FETCH_ASSOC);
        $status["last_update_f"] = date("Y-m-d H:i:s", $status['last_update']);
	$status['sql_debug'] = $sql_cmd;
        header("Content-Type: application/json;charset=utf-8");
//        echo json_encode($status);
        exit;
    }

    if ($device['type'] == "motioneye") {
        $sq = $db->query("SELECT status as device_status,time as last_update,device_id FROM motioneye_devices WHERE device_id = " . $device_id);
        $status = $sq->fetch(PDO::FETCH_ASSOC);
        $status["last_update_f"] = date("Y-m-d H:i:s", $status['last_update']);
        header("Content-Type: application/json;charset=utf-8");
        echo json_encode($status);
        exit;
    }
}

if (isset($_GET["device_status"]) AND ! empty($_GET["device_status"]) AND is_array($_GET['device_status'])) {
    $ids = $_GET['device_status'];
    $db = new PDO('sqlite:' . $config["database"]);
    $data = array();
    for ($i = 0; $i < count($ids); $i++) {
        $device_id = $ids[$i];
        if (!is_numeric($device_id)) {
            continue;
        }
        $qd = $db->query("SELECT type,name,id FROM devices WHERE id = " . $device_id);
        $device = $qd->fetch(PDO::FETCH_ASSOC);
        if ($device["type"] == "mqtt") {

            $sq = $db->query("SELECT response as device_status,last_update,device_id FROM mqtt_devices WHERE device_id = " . $device_id . "");
            $status = $sq->fetch(PDO::FETCH_ASSOC);
            $status["last_update_f"] = date("Y-m-d H:i:s", $status['last_update']);
            $data[$device_id] = $status;
        }

        if ($device['type'] == "motioneye") {
            $sq = $db->query("SELECT status as device_status,time as last_update,device_id FROM motioneye_devices WHERE device_id = " . $device_id);
            $status = $sq->fetch(PDO::FETCH_ASSOC);
            $status["last_update_f"] = date("Y-m-d H:i:s", $status['last_update']);
            $data[$device_id] = $status;
        }
    }
    
    header("Content-Type: application/json;charset=utf-8");
    echo json_encode($data);
    exit;
}


if (isset($_GET['changeState']) AND ! empty($_GET['changeState']) AND isset($_GET['deviceId']) AND is_numeric($_GET['deviceId'])) {

    $device_id = intval($_GET['deviceId']);
    if ($device_id < 0) {
        exit;
    }

    $db = new PDO('sqlite:' . $config["database"]);
    $deviceq = $db->query("SELECT type FROM devices WHERE id = " . $device_id);
    $device = $deviceq->fetch(PDO::FETCH_ASSOC);

    if ($device['type'] == "mqtt") {
        include_once 'jshandlers/mqtt_handler.php';
    }
    ///
}

if (isset($_GET['me']) AND $_GET['me'] == 1 AND isset($_GET['hash']) AND ! empty($_GET['hash'])) {
    $action = "unknown";
    if (isset($_GET['action']) AND ! empty($_GET['action'])) {
        $action = htmlspecialchars($_GET['action']);
        if ($action == "start") {
            $action = "MOTION_START";
        } if ($action == "end") {
            $action = "MOTION_END";
        }
    }
    $db = new PDO('sqlite:' . $config["database"]);
    $hash = htmlspecialchars($_GET['hash']);
    include_once 'libs/devicehistory.class.php';
    $dh = new devicehistory($config);

    $devq = $db->query("SELECT device_id from motioneye_devices WHERE hash = '" . $hash . "'");
    $dev = $devq->fetch(PDO::FETCH_ASSOC);
    $db->exec("update motioneye_devices SET status = '" . $action . "', time=" . time() . " where hash = '" . $hash . "'");
    $dh->addHistoryItem($dev["device_id"], $action);
}

