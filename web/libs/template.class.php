<?php

/*
 * template.class.php
 *
 * Copyright 2011 Szontágh Ferenc <szontagh.ferenc@exahost.eu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

/**
 * class form
 * @author	Ferenc Szontágh <szontagh.ferenc@exahost.eu>
 */
class template {

    var $theme_path, $prefix, $error, $tpl_file, $last_error, $tpl;
    public $cache = true;
    private $cache_dir = 'tmp/cache/';
    private $compile_dir = 'tmp/compiled/';
    private $compiled = '';
    private $compiled_file;
    public $variables = array();
    public $size = 0;
    public $html_tidy = false;
    private $scope = array();
    private $scope_id = 0;
    private $in_scope = false;

    function __construct($theme_path = '', $prefix = '') {
        if (!is_dir($this->cache_dir) AND $this->cache === true) {
            if (!@mkdir($this->cache_dir,0777,true)) {
                $this->error[] = 'Nem sikerült  a cache mappa létrehozása: ' . $this->cache_dir;
            }
        }
        if (!is_dir($this->compile_dir)) {
            if (!@mkdir($this->compile_dir,0777,true)) {
                $this->error[] = 'Nem sikerült a compile mappa létrehozása: ' . $this->compile_dir;
            }
        }
        if (!empty($theme_path)) {
            if (substr($theme_path, -1) != '/') {
                $theme_path .= '/';
            }
            $this->theme_path = $theme_path;
        }
        if (!empty($prefix)) {
            $this->prefix = $prefix;
        }
    }

    public function tidy_html() {
        $d = ob_get_clean();
        $config = array(
            "break-before-br" => true,
            "indent" => true,
            //"indent-attributes"=>true,
            //"indent-spaces"=>true,
            //"markup"=>false,
            "punctuation-wrap" => true,
            "sort-attributes" => false,
            "split" => false,
            "tab-size" => 5,
            "vertical-space" => false,
            "wrap" => 100,
            "wrap-attributes" => true,
            "wrap-jste" => true,
            "wrap-php" => false,
            "wrap-script-literals" => true,
            "wrap-sections" => false
        );
        $tidy = new tidy();
        $tidy->parseString($d, $config);
        $tidy->cleanRepair();
        echo $tidy;
    }

    /* változó hozzáadása */

    public function add($key, $val = null) {
        if (is_array($key)) {
            $this->variables = array_merge($this->variables, $key);
        } elseif ($val != null) {
            $this->variables[$key] = $val;
        }
        return $this->variables;
    }

    /* kimenet */

    public function show($tpl_file, $instance_name = null) {
        $this->tpl_file = $this->theme_path . $tpl_file;
        if (!file_exists($this->tpl_file)) {
            trigger_error("notpl: ".$this->tpl_file,E_USER_ERROR);
        }
        //ob_start();
        $this->tpl = file_get_contents($this->tpl_file);
        //$this->tpl = ob_get_contents();
        //ob_end_clean();
        $this->tpl = preg_replace('/<!--(.*)-->/Uis', '', $this->tpl);
        $this->__parse($instance_name);
        extract($this->variables);
        $this->variables = array();
        include $this->compiled_file;
        $this->size += ob_get_length();
    }

    /* kimenet */

    public function get($tpl_file, $instance_name = null) {
        $this->tpl_file = $this->theme_path . $tpl_file;
        ob_start();
        include $this->tpl_file;
        $this->tpl = ob_get_clean();
        $this->__parse($instance_name);
        extract($this->variables);
        $this->variables = array();
        return $this->compiled;
        $this->compiled;
    }

    /* feldolgozza a változókat */

    private function __parse($instance = null) {
        if (file_exists($this->tpl_file)) {
            if ($instance != null) {
                $this->compiled = $this->__get_instance($instance);
                $this->compiled = preg_replace_callback('/\\{(.*?)\\}/', "template::__parse_statements", $this->compiled);
            } else {
                $this->compiled = preg_replace_callback('/\\{(.*?)\\}/', "template::__parse_statements", $this->__clear_output($this->tpl));
            }

            return $this->__compile($instance);
        } else {
            $this->error[] = 'Nem találom a template fájlt: ' . $this->tpl_file;
            return false;
        }
    }

    private function __get_instance($instance) {
        $tpl = file($this->tpl_file);
        $is_instance = false;
        $ret = '';
        $last_block = '';
        foreach ($tpl as $line) {

            //megkeressük a blokk elejét, ha talált akkor is_instance=true
            if (preg_match('/<!--' . $instance . '-->/', $line)) {
                $is_instance = true;
                $ret .= str_replace("<!--" . $instance . "-->", "", $line);
                continue;
            }

            //megkeressük a végét és leállítjuk a ciklust ha megvan
            if (preg_match('/<!--\/' . $instance . '-->/', $line) AND $is_instance === true) {
                $is_instance = false;
                $ret .= str_replace("<!--/" . $instance . "-->", "", $line);
                break;
            }
            if ($is_instance === true) {
                $ret .= $line;
            }
        }
        if (count($tpl) < 1) {
            echo "Instance nem található!";
            exit;
        } else {
            return $this->__clear_output($this->__removeInlineBlocks($ret));
        }
    }

    private function __removeInlineBlocks($line) {
        return preg_replace('/(<!--[A-Z].*?-->.*?<!--\/[A-Z].*?)-->/', '', $line);
    }

    private function __clear_output($o) {
        return preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $o);
    }

    private function __parse_statements($m) {
        $return = "<?php ";
        if (substr($m[1], 0, 2) == "if") {
            $return .= preg_replace('/if\ (.*)/', 'if ($1):', $m[1]);
            if ($this->in_scope === true) {
                $return = preg_replace('/\$([A-Za-z0-9\_]+)/', "\$this->scope[\$this->scope_id][\"$1\"] ", $return);
            }
        }
        //else
        if (substr($m[1], 0, 4) == "else") {
            $return .= preg_replace('/else/', 'else:', $m[1]);
        }
        //elseif
        if (substr($m[1], 0, 6) == "elseif") {
            $return .= preg_replace('/else\ (.*)/', 'elseif ($1):', $m[1]);
            if ($this->in_scope === true) {
                $return = preg_replace('/\$([A-Za-z0-9\_]+)/', "\$this->scope[\$this->scope_id][\"$1\"]", $return);
            }
        }
        //if vége
        if ($m[1] == "/if") {
            $return .= "endif;";
        }
        //konstans
        if (substr($m[1], 0, 1) == "%") {
            $return .= "echo " . substr($m[1], 1) . ";";
        }
        //loop
        if (substr($m[1], 0, 4) == 'loop') {
            if ($this->in_scope === true) {
                $this->second_scope = 0;
                //$var = "\$this->scope[\$this->scope_id][\"".substr($m[1], 6)."\"]";
                //$return .= "echo \$this->scope_id; foreach(" . $var . " as \$this->scope[\$this->setScope(" . substr($m[1], 6) . ")]):";
                $this->in_scope = true;
                $old_scope_var = "\$this->scope[(\$this->scope_id-" . ($this->second_scope + 1) . ")][\"" . substr($m[1], 6) . "\"]";
                $return .= "\$this->scope_id++; foreach(" . $old_scope_var . " as \$this->scope[\$this->setScope(" . $old_scope_var . ")]):";
            } else {
                $this->in_scope = true;
                $return .= "foreach(" . substr($m[1], 5) . " as \$this->scope[\$this->setScope(" . substr($m[1], 5) . ")]):";
            }
        }
        if ($m[1] == '/loop') {
            $this->in_scope = false;
            if (isset($this->second_scope) AND $this->second_scope > 0) {
                $return .= "endforeach; \$this->endScope((\$this->scope_id-" . $this->second_scope . "));";
            } else {
                if (isset($this->second_scope)) {
                    $this->second_scope++;
                }
                $return .= "endforeach; \$this->endScope(\$this->scope_id);";
            }
        }

        if (substr($m[1], 0, 1) == "$") {
            $return .= "echo " . $m[1] . "; ";
            if ($this->in_scope === true) {
                $return = preg_replace('/\$([A-Za-z0-9\_]+)/', "\$this->scope[\$this->scope_id][\"$1\"]", $return);
            }
        }
        return $return . " ?>";
    }

    /* elkészíti a kész template fájlt	 */

    private function __compile($instance = null) {
        if ($instance != null) {
            $this->compiled_file = $this->compile_dir . $this->theme_path . $instance . ".tpl";
        } else {
            $this->compiled_file = $this->compile_dir . $this->tpl_file;
        }
        if (!file_exists($this->compiled_file)) {
            $f_ = explode("/", $this->compiled_file);
            $file = end($f_);
            $path = explode("/", $this->compiled_file);
            unset($path[(count($path) - 1)]);
            $path = implode("/", $path);
            if (!is_dir($path)) {
                if (!@mkdir($path, 0755, true)) {
                    $this->error[] = 'Nem sikerült létrehozni a template könyvtárat: ' . $path;
                    return false;
                }
            }
        }
        file_put_contents($this->compiled_file, $this->compiled);
        return true;
    }

    private function setScope($data) {
        $this->in_scope = true;
        $this->scope++;
        $id = $this->scope_id;
        $this->scope[$this->scope_id] = $data;
        return $id;
    }

    private function endScope($id) {
        $this->in_scope = false;
        if (isset($id)) {
            unset($this->scope[$id]);
        } else {
            unset($this->scope[$this->scope_id]);
        }
        //$this->scope_id++;
    }

    private function __cache() {
        return false;
    }

    /* utolsó hibaüzenet */

    public function lasterror() {
        if (count($this->error) > 0) {
            return end($this->error);
        }
    }

    /* osztály megsemmisítése */

    function __destruct() {
        if (isset($this->tpl)) {
            unset($this->tpl);
        }
        unset($this->variables);
        unset($this->compiled);
    }

}