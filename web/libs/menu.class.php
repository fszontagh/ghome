<?php

class menu extends navigation {

    var $current_weight = 0;
    private $menu;
    private $id = 0;

    function addMenu($name, $url, $active, $weight, $disabled = false) {
        $nmenu = new menu_item_($name, $url, $active, $weight);
        $nmenu->setDisabled($disabled);
        $nmenu->id = $this->id;
        $this->id++;
        $this->menu[$nmenu->id] = $nmenu;
        unset($nmenu);
        return $this;
    }

    function removeMenu($id) {
        unset($this->menu[$id]);
    }

    function __getIdByName($name) {
        foreach ($this->menu as $idx => $menu) {
            if ($menu->getName() == $name) {
                return $menu->id;
            }
        }
        return false;
    }

    public function listMenu() {
        $data = array();
        foreach ($this->menu as $idx => $menu_item) {
            $data[] = array(
                "name"=>$menu_item->getName(),
                "url"=>$menu_item->getUrl(),
                "active"=>$menu_item->getActive(),
                "disabled"=>$menu_item->getDisabled(),
                "weight"=>$menu_item->getWeight(),
                "id"=>$menu_item->id
                    );
        }
        return $data;
    }

}

class menu_item_ {

    private $name, $url, $active = 0, $weight = 0, $disabled = false;
    var $id = 0;

    public function __construct($name, $url, $active, $weight) {
        $this->name = $name;
        $this->url = $url;
        $this->active = $active;
        $this->weight = $weight;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setUrl($url) {
        $this->url = $url;
    }

    public function setActive($active) {
        $this->active = $active;
    }

    public function setWeight($weight) {
        $this->weight = $weight;
    }

    public function setDisabled($disabled) {
        $this->disabled = $disabled;
    }

    public function getName() {
        return $this->name;
    }

    public function getUrl() {
        return $this->url;
    }

    public function getActive() {
        return $this->active;
    }

    public function getWeight() {
        return $this->weight;
    }

    public function getDisabled() {
        return $this->disabled;
    }

}
