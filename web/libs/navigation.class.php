<?php
class navigation {
    var $cur;
    var $base;
    var $curSite;
    function __construct() {
	 $this->cur = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
//	    $this->cur = "http://ghost001:8881";
        $this->base = preg_replace('/\?.*/', '', $this->cur);
        if (isset($_GET['p']) AND !empty($_GET['p'])) {
            $this->curSite = htmlspecialchars($_GET['p']);
        }
    }
    
    public function reload() {
        header("Location: ".$this->cur);
        exit;
    }
    public function getBase($extra='') {
        return $this->base.$extra;
    }

    public function generateUrl($site) {
        return $this->base."?p=".$site;
    }
    
    public function isActive($site) {
        return $this->curSite===$site?true:false;
    }


    public function redirect($url=null) {
        $url = $url === null?$this->base:$this->base."?p=".$url;
        header("Location: ".$url);
        exit;
    }
    
    
    
    
}
