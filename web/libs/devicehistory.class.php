<?php

class devicehistory {

    var $config, $db;

    public function __construct($config) {
        $this->config = $config;
        $this->db = new PDO('sqlite:' . $config["history_database"]);
    }

    public function addHistoryItem($device_id, $data) {
        $this->db->exec("insert into device_history (device_id,data,time) values (" . $device_id . ",'" . $data . "'," . time() . ")");
        //  $error = $this->db->errorInfo();
    }

    public function listHistory($device_id = 0, $order = " ORDER BY time DESC ", $groupby = " GROUP BY data") {
        if ($device_id == 0) {
            $d = $this->db->query("SELECT * FROM device_history " .$groupby . $order);
            $error = $this->db->errorInfo();
            if (!empty($error[2])) {
                return $error[2];
            }
            return $d;
        }
        $d = $this->db->query("SELECT * FROM device_history where device_id = " . $device_id . " " . $groupby . $order);
        $error = $this->db->errorInfo();
        if (!empty($error[2])) {
            return $error[2];
        }
        return $d;
    }

}
class eventhistory {

    var $config, $db;

    public function __construct($config) {
        $this->config = $config;
        $this->db = new PDO('sqlite:' . $config["history_database"]);
    }

    public function addHistoryItem($id, $data,$type='action') {
        $this->db->exec("insert into ".$type."_history (".$type."_id,data,time) values (" . $id . ",'" . $data . "'," . time() . ")");
        //  $error = $this->db->errorInfo();
    }

    public function listHistory($id = 0,$type='action', $order = " ORDER BY time DESC ", $groupby = " GROUP BY data") {
        if ($device_id == 0) {
            $d = $this->db->query("SELECT * FROM ".$type."_history " .$groupby . $order);
            $error = $this->db->errorInfo();
            if (!empty($error[2])) {
                return $error[2];
            }
            return $d;
        }
        $d = $this->db->query("SELECT * FROM ".$type."_history where device_id = " . $device_id . " " . $groupby . $order);
        $error = $this->db->errorInfo();
        if (!empty($error[2])) {
            return $error[2];
        }
        return $d;
    }

}
