<?php

require("libs/mqtt.class.php");
//query mqtt settings from sql

$mqtt_settings = $db->query("SELECT * from settings where name LIKE 'mqtt_%'");

while ($row = $mqtt_settings->fetch(PDO::FETCH_ASSOC)) {
    $mqtt_broker[$row["name"]] = $row["data"];
}
if ($config["debug"] === true) {
    //print_r($mqtt_broker);
}

$server = $mqtt_broker["mqtt_broker_ip"];     // change if necessary
$port = $mqtt_broker["mqtt_broker_port"];                     // change if necessary
$username = $mqtt_broker["mqtt_broker_username"];                   // set your username
$password = $mqtt_broker["mqtt_broker_password"];                   // set your password
$client_id = $config['mqtt_client_id'] . "_publisher";

//mqtt connection to the device
$mconnq = $db->query("SELECT * FROM mqtt_devices WHERE device_id = " . $device_id . " and controller = 1");
$conn = $mconnq->fetch(PDO::FETCH_ASSOC);



$on_state = trim($conn["on_state_value"]);
$off_state = trim($conn["off_state_value"]);
$new_state = $on_state;
$current_state = $conn['response'];
$control_topic = str_replace("stat/","cmnd/",$conn['topic']);
if ($current_state == $on_state) {
    $new_state = $off_state;
}

header("Content-Type: application/json");
$mqtt = new phpMQTT($server, $port, $client_id);
if ($mqtt->connect(true, NULL, $username, $password)) {
    $mqtt->publish($control_topic, $new_state, 0);
    $mqtt->close();
    echo json_encode(array("device_status"=>$new_state,"status" => "ok", "topic" => $control_topic, "state" => $new_state, "old_state" => $current_state));
} else {
    echo json_encode(array("device_status"=>$current_state,"status" => "timeout", "topic" => $control_topic, "state" => $new_state, "old_state" => $current_state));
}
exit;
