var mqtt = require('mqtt')
var client = mqtt.connect('mqtt://<hostname>')

const sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database("web/data/database.sqlite", (err) => {
    if (err) {
        return console.error(err.message);
    }
   // console.log('Connected to: web/data/database.sqlite');
});

var db_history = new sqlite3.Database("web/data/history.sqlite", (err) => {
    if (err) {
        return console.error(err.message);
    }
  //  console.log('Connected to: web/data/history.sqlite');
});

client.on('connect', function () {
    var t;
    db.all("SELECT topic FROM mqtt_devices WHERE enabled = 1 ", function (err, rows) {
        var t = new Array();
        var c = 0;
        rows.forEach((row) => {
            t[c] = row.topic;
            c++;
        });
        client.subscribe(t);
    });

    //var topic = ["stat/konyha/+", "stat/nagyszoba/+", "stat/folyoso/+"];

})

client.on('message', function (topic, message) {
    //console.log(topic + ": " + message.toString());
     var timestamp = Math.floor(Date.now() / 1000);
    var sql_query = "UPDATE mqtt_devices SET response = '" + message.toString() + "', last_update = "+timestamp+" WHERE topic = '" + topic + "' ";
    db.run(sql_query);

    db.get("select device_id from mqtt_devices WHERE topic = '" + topic + "' ", function (err, row) {
        var timestamp = Math.floor(Date.now() / 1000);
        var sql_query_history = "INSERT INTO device_history (device_id,data,time) values (" + row.device_id + ",'" + message.toString() + "'," + timestamp + ") ";
        db_history.run(sql_query_history)
    });



});

client.on("disconnect", function () {
    db.close((err) => {
        if (err) {
            return console.error(err.message);
        }
      //  console.log('Close the database connection.');
    });
});

